import { AuthChecker, ResolverData } from 'type-graphql';
import { Request, Response} from 'express'
import jwt from 'jsonwebtoken';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Repository, getRepository } from 'typeorm';
import { Vault } from './entities/vault';
import { Hero } from './entities/hero'

const JWT_SIGNING_SECRET = 'this_secret_should_come_from_hidden_env_files_and_never_committed_to_repo';

interface DecodedToken {
  userId: string
}


export const customAuthChecker: AuthChecker<{req: Request, res: Response}> = async ( { context }, roles ) => {
  
  // Get the token from the request hesders
  const token = context.req.headers.authorization;

  // Decode token to get user id associated with that token
  let verifyResult: boolean;
  
  // Verify the token
  await jwt.verify(token, JWT_SIGNING_SECRET, async (err, decode: DecodedToken) => {
    console.log("jwt verify callback")
    if(err){
      // if jwt is not valid deny access
      console.log("error");
      verifyResult = false;
      return;
    }

    // get the hero associated with this id
    const userId = Number(decode.userId);
    const heroRepository = getRepository(Hero);
    const hero = await heroRepository.findOne({ id: userId }) // Wait for the promise to resolve
    
    // Check if current hero has proper authorization
    console.log("hero", hero);
    if(roles.includes(hero.role)){
      verifyResult = true;
      return;
    }
    else{
      verifyResult = false;
      return;
    }
  });

  return verifyResult;
};