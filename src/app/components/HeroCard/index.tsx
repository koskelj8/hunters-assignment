// It is your job to implement this. More info in README

import * as React from 'react'
import styled from 'styled-components'

interface IHeroCardProps {
  name: string
  imgUrl: string
  description: string
}

/*
height: 280px;
  width: 280px;
  border-radius: 20px;
  margin-left: 10px;
  margin-right: 10px;
  margin-top: 0;
  margin-bottom: 0;
  box-sizing: border-box;
  overflow: hidden;
  box-shadow: 2px 4px 4px #888888;
  display: flex;
  flex-direction: column;
  position: relative;
*/
//background-color: #191919;
const CardContainer = styled.div`
  height: 400px;  
  width: 400px;
  border-radius: 20px;
  margin-left: 20px;
  margin-righ: 20px;
  margin-top: 0;
  margin-bottom: 0;
  box-sizing: border-box;
  overflow: hidden;
  box-shadow: 2px 4px 4px #888888;
  display: flex;
  flex-direction: column;
  position: relative;
`

const CollapsibleButton = styled.div`
  font-family: "Montserrat";
  background-color: #eee;
  color: #444;
  cursor: pointer;
  width: px;
  height: 150px;
  padding-left: 10px;
  padding-right: 10px;
`
/*
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
*/

const StatsContainer = styled.div`
  font-family: "Montserrat";
  width: 400px;
  height: 150px;
  padding-left: 10px;
  padding-right: 10px;
`

const NameContainer = styled.h1`
  font-family: "Montserrat";
  font-weight: 500;
  position: absolute;
  left: 10px;
  top: 2px;
  z-index: 2;
  color: white;
  text-shadow: 2px 2px 2px black;
`
/* name
			imgUrl
			description
			backStory
			attributes {
				strength
				intelligence
				stamina
				healthpoints
				mana
				agility
				speed
				resistance
				weakness
			}
			skills {
				name
				damage
				element
			} */

export const HeroCard: React.FC<IHeroCardProps> = ({ name, imgUrl, description }) => {
  return (
    <CardContainer>
      <NameContainer>{name}</NameContainer>
      <img src={imgUrl} alt="Hero Image" />
      <StatsContainer>
        Description{description}
      </StatsContainer>    
    </CardContainer>
  )
}
